package cat.dam.rocriba.sqlitetone;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityGuitarraEspanyola3 extends AppCompatActivity {
    DatabaseHelper databaseHelper;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guitarraespanyola3);
        //Inicialize all the variables we need
        TextView nominstrument =(TextView) findViewById(R.id.tv_nom9);
        ImageView imatgeinstrumment = findViewById(R.id.iv_instrument9);
        TextView descripcio = (TextView) findViewById(R.id.tv_descripcio9);
        TextView preu = (TextView) findViewById(R.id.tv_preu9);

        //Create the object for the database
        databaseHelper = new DatabaseHelper(this);
        //Extract all the data that we need from the database
        Cursor cursor = databaseHelper.GetSelectData("guitarres",9);
        cursor.moveToNext();
        //Put the data in the textViews and Images
        nominstrument.setText(cursor.getString(1));
        imatgeinstrumment.setImageResource(R.drawable.guitarraespanyola3);
        descripcio.setText(cursor.getString(3));
        String money = cursor.getInt(4) + "€";
        preu.setText(money);
    }
}
