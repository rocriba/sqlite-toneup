package cat.dam.rocriba.sqlitetone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityDuo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duo);

        //Incialitze Variables
        Button afegir = findViewById(R.id.btn_afegir);
        Button eliminar = findViewById(R.id.btn_eliminarr);


        //If button afegir is slected we start a new activity for the user can add data
        //to the database
        afegir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityDuo.this, ActivityAfegir.class);
                startActivity(intent);
            }
        });


        //If the user select this button we launch, to another activity for delete instruments from database

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityDuo.this, ActivityEliminar.class);
                startActivity(intent);
            }
        });
    }
}
