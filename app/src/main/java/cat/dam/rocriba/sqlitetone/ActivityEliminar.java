package cat.dam.rocriba.sqlitetone;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityEliminar extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eliminar);

        //Inicalize Variables
        EditText nomInstrument = findViewById(R.id.et_nominstrumenteliminat);
        EditText instrument = findViewById(R.id.et_tipusintrumentel);
        Button eliminar = findViewById(R.id.btn_eliminarr3);


        //Inicialize Object
        databaseHelper = new DatabaseHelper(this);

        //If the Button is selected we check the data entered an try to delete instrument,
        //showing if we did it or not
        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (!databaseHelper.eliminarInstrument(nomInstrument.getText().toString(), instrument.getText().toString())){
                   Toast.makeText(getApplicationContext(),"Dades introduiedes incorrectament", Toast.LENGTH_SHORT).show();
                } else {
                   Toast.makeText(getApplicationContext(),"Instrument " + nomInstrument.getText() + " borrat correctament", Toast.LENGTH_SHORT).show();

               }
            }
        });




    }
}
