package cat.dam.rocriba.sqlitetone;


import android.database.Cursor;

//Class that gets the data from the database and return it's data
public class DataModel {
    private int imageDrawable;
    private String title;
    private String subTitle;
    private String button;




    public DataModel(int id, Cursor cursor, int imatge, String activity) {

        cursor.moveToNext();

        imageDrawable = imatge;

        title = cursor.getString(1);

        subTitle = cursor.getString(4)+ "€";

        button = activity;
    }


    public int getImageDrawable(){
        return imageDrawable;

    }

    public String getTitle(){
        return title;
    }

    public String getSubTitle(){
        return subTitle;
    }

    public String getButton(){return button;}
}
