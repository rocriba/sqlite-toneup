package cat.dam.rocriba.sqlitetone;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class RegisterActivity extends AppCompatActivity {
    //Inicalize variables
    EditText usuari, contrasenya;
    Button register, tornar;
    DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //Inicalize Variables
        usuari = findViewById(R.id.et_newusuari);
        contrasenya = findViewById(R.id.et_newpassword);
        register = (Button) findViewById(R.id.btn_alta);
        tornar = (Button) findViewById(R.id.btn_tornar);
        //Inicialize Object
        databaseHelper = new DatabaseHelper(RegisterActivity.this);

        //Button that when is pressed returns to the main activity
        tornar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        //Button that when is presed check if user exists, and if not, add it to the database
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = usuari.getText().toString();
                String password = contrasenya.getText().toString();
                String missatge = databaseHelper.addUser(username, password);

                Toast.makeText(getApplicationContext(),missatge, Toast.LENGTH_SHORT).show();

            }
        });



    }
}
