package cat.dam.rocriba.sqlitetone;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.strictmode.SqliteObjectLeakedViolation;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;

public class DatabaseHelper extends SQLiteOpenHelper {

   //Inicalizing variables
    public static final String DATABASE_NAME = "usuaris";
    public static final int DATABASE_VERSION = 1;
    public static final String USER_TABLE = "usuaris";
    private final String TABLE_NAME = "usuaris";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Method that creates database if it's not created
    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
            db.execSQL("CREATE TABLE usuaris (id INTEGER PRIMARY KEY, nom TEXT, password TEXT);");
            db.execSQL("INSERT INTO usuaris values (1, 'admin', 'admin');");
            db.execSQL("INSERT INTO usuaris values (2, 'roc', 'roc');");
            db.execSQL("CREATE TABLE instru (ID INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT, image BLOB);");
            db.execSQL("INSERT INTO instru values (1, 'test', '123');");
            db.execSQL("CREATE TABLE imatges (id INTEGER PRIMARY KEY AUTOINCREMENT, imatge BLOB, descripcio TEXT );");
            db.execSQL("INSERT INTO imatges VALUES (1, '123', 'prova');");
            db.execSQL("CREATE TABLE guitarres (id INTEGER PRIMARY KEY, nom TEXT, tipus TEXT, informacio TEXT, preu INTEGER, imatge BLOB, imatged TEXT);");
            db.execSQL("INSERT INTO guitarres values (1, 'Harley Benton SC-400 SBK Vintage Series', 'electrica', 'Cuerpo de tilo con tapa arqueada\n" +
                    "Mástil encolado de arce forma C\n" +
                    "Diapasón de Roseacer\n" +
                    "Radio del diapasón: 350 mm\n" +
                    "Puente Tune-o-matic', 138, '123', 'R.drawable.guitelectrica');");

            db.execSQL("INSERT INTO guitarres values (2, 'Harley Benton SC-1000 WH Progressive Line', 'electrica', 'Construcción del mástil: Encolada (set-neck)\n" +
                    "Cuerpo de caoba\n" +
                    "Tapa arqueada\n" +
                    "Mástil de caoba con forma de C\n" +
                    "Diapasón de amaranto con inlays', 198, '123', 'R.drawable.guitelectrica2');");

            db.execSQL("INSERT INTO guitarres values (3, 'Harley Benton SC-450Plus VB Vintage Series', 'electrica', 'Vintage Series\n" +
                    "Cuerpo de caoba con tapa arqueada de arce flameado de grado AAA\n" +
                    "Mástil encolado de caoba con perfil C\n" +
                    "Diapasón de Pau Ferro\n', 198, '123', 'R.drawable.guitarraelectrica3');");

            db.execSQL("INSERT INTO guitarres values (4, 'Harley Benton GS-Travel Mahogany', 'acustica', 'Tamaño Grand Symphony mini de viaje\n" +
                    "Cuerpo de caoba seleccionada con fondo curvado\n" +
                    "Mástil de nato en forma de V\n" +
                    "Diapasón de granadilla\n" +
                    "Radio del diapasón: 381 mm\n" +
                    "Escala de 596 mm\n" +
                    "Anchura de la cejilla: 43 mm\n" +
                    "20 trastes\n" +
                    "Inlays de puntos\n" +
                    "Puente de granadilla\n" +
                    "Bindings del cuerpo y del mástil en negro', 98, '123', 'R.drawable.guitacustica');");

            db.execSQL("INSERT INTO guitarres values (5, 'Harley Benton GS-Travel-E Mahogany', 'acustica', 'Tamaño Gran Symphony mini de viaje\n" +
                    "Cuerpo de caoba seleccionada con fondo curvado\n" +
                    "Mástil de nato en forma de V\n" +
                    "Diapasón de granadilla\n" +
                    "Escala de 596 mm\n" +
                    "Anchura de la cejilla: 43 mm\n" +
                    "20 trastes\n" +
                    "Inlays de puntos\n" +
                    "Puente de granadilla', 118, '123', 'R.drawable.guitarraelectrica2');");

            db.execSQL("INSERT INTO guitarres values (6, 'PRS SE P20E Parlor PB LTD', 'acustica', 'Edición limitada\n" +
                    "Estilo Tonare\n" +
                    "Tapa de caoba maciza\n" +
                    "Cuerpo de caoba\n" +
                    "Mástil de caoba\n" +
                    "Diapasón de ébano\n" +
                    "Perfil del mástil ancho grueso\n" +
                    "Refuerzos en X híbridos tradicionales\n" +
                    "Binding del cuerpo de espina de pescado en color crema\n" +
                    "20 trastes', 559, '123', 'R.drawable.guitarraelectrica3');");
            db.execSQL("INSERT INTO guitarres values (7, 'Yamaha C49', 'espayola', 'apa de pícea\n" +
                    "Aros y fondo de meranti (Shorea macrophylla)\n" +
                    "Mástil de nato\n" +
                    "Diapasón de palisandro (Dalbergia latifolia)\n" +
                    "Puente de palisandro (Dalbergia latifolia)\n" +
                    "Ancho de la selleta: 52mm\n" +
                    "Profundidad del cuerpo: 49-100mm\n" +
                    "Longitud de la escala: 650mm/25.59\"\n" +
                    "Acabado: Natural (brillante)', 149, '123', 'R.drawable.guitclassica');");

            db.execSQL("INSERT INTO guitarres values (8, 'Yamaha C40', 'espayola', 'apa de pícea\n" +
                    "Aros y fondo de meranti (Shorea macrophylla)\n" +
                    "Mástil de nato\n" +
                    "Diapasón de palisandro (Dalbergia latifolia)\n" +
                    "Puente de palisandro (Dalbergia latifolia)\n" +
                    "Ancho de la selleta: 52mm\n" +
                    "Profundidad del cuerpo: 49-100mm\n" +
                    "Longitud de la escala: 650mm/25.59\"\n" +
                    "Acabado: Natural (brillante)', 122, '123', 'R.drawable.guitarraespanyola2');");

            db.execSQL("INSERT INTO guitarres values (9, 'Yamaha C40 BL', 'espayola', 'apa de pícea\n" +
                    "Aros y fondo de meranti (Shorea macrophylla)\n" +
                    "Mástil de nato\n" +
                    "Diapasón de palisandro (Dalbergia latifolia)\n" +
                    "Puente de palisandro (Dalbergia latifolia)\n" +
                    "Ancho de la selleta: 52mm\n" +
                    "Profundidad del cuerpo: 49-100mm\n" +
                    "Longitud de la escala: 650mm/25.59\"\n" +
                    "Acabado: Natural (brillante)', 138, '123', 'R.drawable.guitarraespanyola3');");

            db.execSQL("CREATE TABLE bateries (id INTEGER PRIMARY KEY, nom TEXT, tipus TEXT, informacio TEXT, preu INTEGER, imatge BLOB, imatged TEXT);");

            db.execSQL("INSERT INTO bateries values (1, 'Millenium MPS-850 E-Drum Set', 'electrica', '550 sonidos\n" +
                    "30 Kits predefinidos\n" +
                    "20 Kits de usuario\n" +
                    "100 canciones\n" +
                    "2 canciones de usuario\n" +
                    "Función Quick Record\n" +
                    "Metrónomo\n" +
                    "Ecualizador para cada Kit\n" +
                    "Pitch\n" +
                    "Reverb\n" +
                    "Compresor', 598, '123', 'R.drawable.bateria');");

            db.execSQL("INSERT INTO bateries values (2, 'Millenium HD-120 E-Drum Set', 'electrica', 'et completo ideal para principiantes\n" +
                    "Gracias a sus medidas compactas y el poco espacio que necesita es ideal también para niños\n" +
                    "Necesita muy poco espacio de montaje - superficie necesaria: 100 x 60 cm\n" +
                    "12 kits de batería\n" +
                    "Metrónomo de 40-240 bpm\n" +
                    "Salida de linea estéreo minijack de 3,5 mm\n" +
                    "Salida de auriculares estéreo minijack de 3,5 mm\n" +
                    "Entrada auxiliar estéreo minijack de 3,5 mm\n" +
                    "USB-MIDI', 219, '123', 'R.drawable.bateriaeletrica2');");

            db.execSQL("INSERT INTO bateries values (3, 'Millenium MPS-150 E-Drum Set', 'electrica', '108 sonidos\n" +
                    "10 kits de batería\n" +
                    "40 canciones\n" +
                    "Metrónomo con tempo de 30 a 280 BPM\n" +
                    "Reverb\n" +
                    "2 salidas mono jack de 1/4\"\n" +
                    "Entrada de línea estéreo jack de 1/8\"', 249, '123', 'R.drawable.bateriaeletrica3');");

            db.execSQL("INSERT INTO bateries values (4, 'Millenium Focus Junior Drum Set Black', 'acustica', 'Bombo de 16\" x 14\"\n" +
                    "Tom de 08\" x 05\"\n" +
                    "Tom de 10\" x 06\"\n" +
                    "Tom de suelo de 13\" x 11\"\n" +
                    "Caja de madera de 12\" x 05\"', 149, '123', 'R.drawable.bateriaacustica1');");

            db.execSQL("INSERT INTO bateries values (5, 'Millenium Youngster Drum Set Silver', 'acustica', 'Bombo de 16\" x 12\"\n" +
                    "Tom de 08\" x 05\"\n" +
                    "Caja de 10\" x 04\"\n" +
                    "Soporte para caja\n" +
                    "Soporte de hi-hat\n" +
                    "Pedal\n" +
                    "Taburete\n" +
                    "Baquetas\n" +
                    "Platillos LowNoiz, los cuales destacan por su menor volumen sonoro y su decaimiento rápido', 99, '123', 'R.drawable.bateriaacustica2');");

            db.execSQL("INSERT INTO bateries values (6, 'Millenium MX422 Standard Set RL', 'acustica', 'Versión Standard\n" +
                    "Acabado: Red Lining\n" +
                    "Cascos de 9 capas de álamo/abedul recubiertos\n" +
                    "Aros de metal de 1,5 mm en los toms y la caja\n" +
                    "Aro de madera a juego en el bombo', 349, '123', 'R.drawable.bateriaacustica3');");

            db.execSQL("CREATE TABLE baixos (id INTEGER PRIMARY KEY, nom TEXT, tipus TEXT, informacio TEXT, preu INTEGER, imatge BLOB, imatged TEXT);");

            db.execSQL("INSERT INTO baixos values (1, 'Marcus Miller V7 Swamp Ash-4 NT 2nd Gen', 'normal', 'Fabricado por Sire\n" +
                    "Cuerpo de fresno de pantano\n" +
                    "Mástil de 1 pieza de arce con perfil en C\n" +
                    "Diapasón de arce\n" +
                    "Radio del diapasón de 9,5\"\n" +
                    "Escala larga: 34\"\n" +
                    "20 trastes Medium Small\n" +
                    "Ancho de la cejilla: 38 mm\n" +
                    "Espaciado entre cuerdas: 20 mm\n" +
                    "Cejilla de hueso\n" +
                    "2 pastillas Marcus Super-J Revolution Jazz\n', 498, '123', 'R.drawable.baixnormal1');");
            db.execSQL("INSERT INTO baixos values (2, 'Harley Benton JB-20 SB Standard Series', 'normal', 'Standard Series\n" +
                    "Construcción del mástil: Atornillada (bolt-on)\n" +
                    "Cuerpo de álamo\n" +
                    "Mástil moderno de arce en forma de C\n" +
                    "Diapasón de amaranto con inlays de puntos\n" +
                    "20 trastes\n" +
                    "Escala de 864 mm\n" +
                    "Ancho de la cejilla: 38 mm\n" +
                    "Alma de doble acción\n" +
                    "2 pastillas de bobina simple de estilo JB', 98, '123', 'R.drawable.baixnormal2');");
            db.execSQL("INSERT INTO baixos values (3, 'Marcus Miller V7 Alder-4 BK 2nd Gen', 'normal', 'Fabricado por Sire\n" +
                    "Cuerpo de aliso\n" +
                    "Mástil de 1 pieza de arce con perfil en C\n" +
                    "Diapasón de ébano\n" +
                    "Radio del diapasón de 9,5\n" +
                    "Escala larga: 34\\n" +
                    "20 trastes Medium Small\n" +
                    "Ancho de la cejilla: 38 mm\n" +
                    "Espaciado entre cuerdas: 20 mm\n" +
                    "Cejilla de hueso\n" +
                    "2 pastillas Marcus Super-J Revolution Jazz\n" +
                    "Electrónica Marcus Heritage-3 con control de frecuencia\n', 495, '123', 'R.drawable.baixnormal3');");

            db.execSQL("INSERT INTO baixos values (4, 'Furch Bc61-5 CM Acoustic Bass', 'acustic', 'apa de arce\n" +
                    "Aros y fondo de caoba\n" +
                    "Mástil de caoba\n" +
                    "Diapasón de pino de Nueva Zelanda\n" +
                    "Anchura de la cejilla: 47mm\n" +
                    "22 trastes\n" +
                    "Pastilla Fishman Sonicore\n" +
                    "Preamplificador Ibanez AEQ-SP2 con afinador incorporado\n" +
                    "Roseta de abulón\n" +
                    "Pivotes del puente de plástico negro\n" +
                    "Escala: 812,8mm\n" +
                    "Acabado de alto brillo', 1447, '123', 'R.drawable.baixacustic1');");

            db.execSQL("INSERT INTO baixos values (5, 'Ibanez AEB105E-NT Acoustic Bass', 'acustic', 'Durango Series\n" +
                    "Con cutaway veneciano\n" +
                    "Tapa de cedro macizo\n" +
                    "Aros y fondo de sapeli\n" +
                    "Mástil de caoba\n" +
                    "Diapasón de ébano\n" +
                    "Escala: 860mm\n" +
                    "Sistema L.R. Baggs EAS-VTC\n" +
                    "Acabado: Natural satinado\n" +
                    "Incluye funda\n" +
                    "Estuche opcional (artículo nº178270) no incluido', 407, '123', 'R.drawable.baixacustic2');");

            db.execSQL("INSERT INTO baixos values (6, 'Warwick Alien Deluxe 5 Hybrid Thinline', 'acustic', 'Cuerpo Thin-line\n" +
                    "Escala larga de 34\"\n" +
                    "Profundidad del cuerpo: 80 mm (3,15)\n" +
                    "Construcción del mástil: Encolada (set-neck)\n" +
                    "Tapa de pícea de Sitka maciza\n" +
                    "Aros y fondo de nogal\n" +
                    "Mástil de caoba\n" +
                    "Diapasón de wengué\n" +
                    "Radio del diapasón: 26\n" +
                    "24 trastes jumbo extra-altos de alpaca\n" +
                    "Cejilla Just a Nut III de Tedur', 925, '123', 'R.drawable.baixacustic3');");

            db.execSQL("CREATE TABLE pianos (id INTEGER PRIMARY KEY, nom TEXT, tipus TEXT, informacio TEXT, preu INTEGER, imatge BLOB, imatged TEXT);");

            db.execSQL("INSERT INTO pianos values (1, 'Thomann DP-26', 'escenari', '88 Weighted keys with hammer action keyboard\n" +
                    "20 Sounds\n" +
                    "2 Demo songs\n" +
                    "50 Styles\n" +
                    "128 Voice polyphony\n" +
                    "LED display\n" +
                    "Layer mode\n" +
                    "Split mode\n" +
                    "Duo mode\n" +
                    "Master EQ\n" +
                    "Reverb\n" +
                    "Chorus', 299, '123', 'R.drawable.painoescenari1');");

            db.execSQL("INSERT INTO pianos values (2, 'Thomann SP-5600', 'escenari', '88 teclas de acción martillo\n" +
                    "600 sonidos\n" +
                    "230 estilos (10 estilos de usuario)\n" +
                    "120 canciones\n" +
                    "128 notas de polifonía\n" +
                    "Control de acompañamiento: Start/Stop, Sync Start, Intro/Ending, Fill A y Fill B\n" +
                    "Funciones: Duo (Twinova), Split, Layer y Sustain\n" +
                    "Función One Touch Setting\n" +
                    "Metrónomo\n" +
                    "Efectos DSP\n" +
                    "Ecualizador master\n" +
                    "Reverb y Chorus\n" +
                    "Secuenciador\n" +
                    "Asistente de interpretación\n" +
                    "Armonía', 299, '123', 'R.drawable.painoescenari2');");

            db.execSQL("INSERT INTO pianos values (3, 'Korg B2 Black', 'escenari', 'Teclado de 88 teclas con mecánica de martillos\n" +
                    "3 curvas de sensibilidad dinámica (ligera, normal, pesada)\n" +
                    "12 sonidos\n" +
                    "Polifonía de 120 voces\n" +
                    "Sistema de altavoces de 2x 15 W\n" +
                    "Entrada de línea estéreo minijack de 3.5 mm\n" +
                    "Salida combinada de línea/auriculares minijack estéreo de 3.5 mm\n" +
                    "USB (MIDI/Audio)\n" +
                    "Conexión de pedal\n" +
                    "Entrada para fuente de alimentación externa\n" +
                    "Medidas (An x Al x Pr): 1312 × 117 x 336 mm\n" +
                    "Peso: 11,4 kg\n" +
                    "Color: Negro', 398, '123', 'R.drawable.painoescenari3');");

            db.execSQL("INSERT INTO pianos values (4, 'Thomann DP-26', 'digitals', '88 teclas contrapesadas de acción martillo\n" +
                    "20 sonidos\n" +
                    "2 canciones demo\n" +
                    "Polifonía de 128 notas\n" +
                    "Pantalla LED\n" +
                    "Modo por capas\n" +
                    "Modo de partición\n" +
                    "50 estilos\n" +
                    "Ecualizador master\n" +
                    "Reverb\n" +
                    "Chorus', 299, '123', 'R.drawable.pianodigital1');");

            db.execSQL("INSERT INTO pianos values (5, 'Korg B2 Black', 'digitals',' Teclado de 88 teclas con mecánica de martillos\n" +
                    "3 curvas de sensibilidad dinámica (ligera, normal, pesada)\n" +
                    "12 sonidos\n" +
                    "Polifonía de 120 voces\n" +
                    "Sistema de altavoces de 2x 15 W\n" +
                    "Entrada de línea estéreo minijack de 3.5 mm\n" +
                    "Salida combinada de línea/auriculares minijack estéreo de 3.5 mm\n" +
                    "USB (MIDI/Audio)\n" +
                    "Conexión de pedal\n" +
                    "Entrada para fuente de alimentación externa', 398, '123', 'R.drawable.pianodigital1');");

            db.execSQL("INSERT INTO pianos values (6, 'Thomann DP-33 WH', 'digitals', '88 teclas de acción martillo con sensibilidad al tacto\n" +
                    "26 sonidos\n" +
                    "64 notas de polifonía\n" +
                    "60 canciones\n" +
                    "Modo dual y split\n" +
                    "Reverb\n" +
                    "Chorus\n" +
                    "Metrónomo\n" +
                    "Función de transposición\n" +
                    "Ecualizador master (3 tipos)\n" +
                    "Librería de música con 60 canciones preset\n" +
                    "Sistema de altavoces 2 x 15W\n" +
                    "3 pedales\n', 398, '123', 'R.drawable.pianodigital1');");

            db.execSQL("INSERT INTO pianos values (7, 'Kawai K-300 E/P Piano', 'vertical', '88 Teclas\n" +
                    "3 Pedales\n" +
                    "Acción Vertical Millennium III exclusiva con piezas de ABS-Carbono\n" +
                    "Martillos Double Delted\n" +
                    "Martillos con núcleo de caoba\n" +
                    "Dimensiones: 122 x 149 x 61 cm\n" +
                    "Peso: 227 kg\n" +
                    "Color: Negro pulido', 5444, '123', 'R.drawable.pianovertical1');");

            db.execSQL("INSERT INTO pianos values (6, 'Yamaha b1 PE', 'vertical', 'El B1 es un ejemplo que materializa los principios de construcción de Yamaha, donde el concepto de \"valor\" se redefine. El estándar de calidad de Yamaha aplicado a cada detalle son orgullo de cada propietario de un b1, manteniendo al tiempo una excelente relación entre calidad y prestaciones.', 3377, '123', 'R.drawable.pianovertical2');");

            db.execSQL("INSERT INTO pianos values (7, 'Kawai K 15 ATX 3-L E/P Piano', 'vertical', 'Gracias a la combinación de espesor del material, estabilidad, precisión y fiabilidad, las mecánicas Kawai \"Ultra-Responsive Action\" son líderes. Las cabezas de los martillos son prensadas con 100% algodón \"Premium\"', 5333, '123', 'R.drawable.pianovertical3');");


            db.execSQL("INSERT INTO pianos values (8, 'Kawai GL 10 E/P Grand Piano', 'cola', 'Mecánica ultrasensible\n" +
                    "Martillos con fieltro en parte inferior\n" +
                    "Tapa del teclado de cierre suave\n" +
                    "Longitud: 153cm\n" +
                    "Acabado: Negro pulido\n" +
                    "Peso: 282Kg', 9555, '123', 'R.drawable.pianocola1');");

            db.execSQL("INSERT INTO pianos values (9, 'Yamaha GC 1 M PE Grand Piano', 'cola', 'Mecánica ultrasensible\n" +
                    "Martillos con fieltro en parte inferior\n" +
                    "Tapa del teclado de cierre suave\n" +
                    "Longitud: 153cm\n" +
                    "Acabado: Negro pulido\n" +
                    "Peso: 282Kg', 16390, '123', 'R.drawable.pianocola2');");

            db.execSQL("INSERT INTO pianos values (10, 'Yamaha GC 1 TA2 PE Grand Piano', 'cola', 'Con función transacústica\n" +
                    "Puedes tocar sonidos digitales sin auriculares o altavoces\n" +
                    "La caja resonancia de madera es el altavoz\n" +
                    "Este piano combina la escala dúplex y el rico carácter tonal del codiciado C1 Grand con ventajas de ahorro de costos en materiales y producción, para crear un instrumento que es excepcionalmente expresivo y extraordinariamente asequible\n" +
                    "Largo: 161 cm\n" +
                    "Acabado: Negro pulido', 24890, '123', 'R.drawable.pianocola3');");



        } catch (SQLException e){
           e.printStackTrace();
        }

    }

    //Method that we change variable DATABASE VERSION automaticly onUpgrade is executed for change the version of database, adding new features
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS  usuaris;");
        db.execSQL("DROP TABLE IF EXISTS instruments;");
        db.execSQL("DROP TABLE IF EXISTS instru;");
        db.execSQL("DROP TABLE IF EXISTS guitarres;");
        db.execSQL("DROP TABLE IF EXISTS imatges");
        db.execSQL("DROP TABLE IF EXISTS bateries");
        db.execSQL("DROP TABLE IF EXISTS baixos");
        db.execSQL("DROP TABLE IF EXISTS pianos");


        onCreate(db);
    }

    //Boleean function that returns true or false depending if user exists
    public boolean CheckIfUserExists(String username, String password){

        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {"nom"};

        String selection = "nom=? and password= ?";
        String[] selectionArgs = {username, password};
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE nom = '" + username + "' AND password = '" + password +"';";
       Cursor cursor = db.query(USER_TABLE, columns, selection, selectionArgs, null, null, null);
        int count = cursor.getCount();
        cursor.close();
        if(count > 0){
            return true;
        } else {
            return false;
        }
    }

    //Pubilic method that adds user to the database
    public String addUser (String username, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        String[] columns = {"nom"};
        String selection = "nom=? and password= ?";
        String[] selectionArgs = {username, password};
        Cursor cursor = db.query(USER_TABLE, columns, selection, selectionArgs, null, null, null);
        int count = cursor.getCount();
        String query = "SELECT * FROM " + TABLE_NAME +";";
        Cursor cid = db.rawQuery(query,null);
        int id = cid.getCount()+1;
        cursor.close();
        if(count > 0){
            return "L'usuari ja existeix";
        } else {
            db.execSQL("INSERT INTO " + TABLE_NAME + " VALUES (" + id +", '"+ username +"', '" + password + "');");
            return "Usuari: " + username + " afegit correctament";
        }

    }

    //Method that tries to insert image in database using bitmap and ByteArrayOutputStrean
    public  void Insertimage (byte[] image, String Imagename){
        SQLiteDatabase db = this.getWritableDatabase();
/*
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

 */
        String query = "SELECT * FROM instru;";
        Cursor cid = db.rawQuery(query,null);
        int id = cid.getCount()+1;

        db.execSQL("INSERT INTO instru VALUES (" + id +", '" + Imagename + "', '" + image + "');");




    }
    //Public method that selects a image from the database and retuns that as a byte array
    public byte[] SelectImage ( String NomImatge){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM  instru WHERE nom = '"+NomImatge+"';", null, null);
        cursor.moveToNext();
            byte[] getImatge = cursor.getBlob(2);
        return getImatge;


    }

    //public mehtod that gets all info about from a row and upper
    public Cursor GetAllData(String nomInstrument, int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + nomInstrument +" WHERE id >= " + id +";", null);
        return c;
    }

    //Public function that selects data fro one row, specifing it form the id
    public Cursor GetSelectData(String nomInstrument, int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + nomInstrument +" WHERE id = " + id +";", null);
        return c;
    }

    //Public method that inserts image bitmap into the database
    public void insertBitmap (Bitmap bm) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, out);
        byte[] buffer = out.toByteArray();
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ContentValues values;

        try {
            values = new ContentValues();
            values.put("img", buffer);
            long i = db.insert("imatge", null, values);
            db.setTransactionSuccessful();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        db.endTransaction();

    }

    //Public function that selects bitmap from the database
    public Bitmap getBitmap(String taula, int id) {
        Bitmap bitmap = null;
        SQLiteDatabase db = this.getReadableDatabase();
        db.beginTransaction();
        try{
            String selectQuery = "SELECT * FROM " + taula + " WHERE id = " + id +";";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0){
                cursor.moveToNext();
                byte[] blob = cursor.getBlob(1);
                bitmap =BitmapFactory.decodeByteArray(blob, 0, blob.length);
            }
            db.setTransactionSuccessful();
        } catch (SQLiteException e)
        {
            e.printStackTrace();

        }
        db.endTransaction();
        return bitmap;
    }
    //Boleean function that inserts instrument to the database, returns true if we can if not, false
    public boolean insertInstrument(String table, String nomIns, String tipusIns, String descripcioIns, String preuins, Bitmap foto ){
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            String query = "SELECT * FROM " + table +";";
            Cursor cid = db.rawQuery(query,null);
            int id = cid.getCount()+1;
            db.execSQL("INSERT INTO" + table + " (id, nom, tipus, informacio , preu , imatge BLOB, ) VALUES (" + id +", '"+ nomIns +"', '" + tipusIns + "', '" + descripcioIns +"', '" + preuins + "', " + foto + ");");
    return true;
        } catch (SQLiteException e){
            return false;
        }  }
        // public function that deletes one instrument, if he can, returns true, if not false
    public boolean eliminarInstrument (String nom, String table){
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.execSQL("DELETE FROM " + table + " WHERE nom = '" + nom + "';");
            return true;
        } catch (SQLException e){
            return  false;
        }

    }
}

