package cat.dam.rocriba.sqlitetone;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

public class DetectorConnexio {
   //Class that detects connection
    private Context mContext;
    //constructor
    public DetectorConnexio(Context context) {
        this.mContext = context;
    }
    // Check connection (wifi and data mobile)
    public boolean teConnexio() {
        final ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            if (Build.VERSION.SDK_INT >= 23) {
                //CHeck version
                //Bigger versions for SDK 23
                final Network network = connectivityManager.getActiveNetwork();
                if (network != null) {
                    final NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(network);
                    return (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                            networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI));
                }
            } else {
                //Lower versions for SDK 23
                final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                if (networkInfo != null) {
                    return (networkInfo.isConnected() && (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE ||
                            networkInfo.getType() == ConnectivityManager.TYPE_WIFI));
                }
            }
        }
        return false;
    }
}