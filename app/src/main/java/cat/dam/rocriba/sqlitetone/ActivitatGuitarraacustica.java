package cat.dam.rocriba.sqlitetone;

import android.database.Cursor;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ActivitatGuitarraacustica extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    DatabaseHelper databaseHelper;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guitarraacustica);

        //Inicialitze objects
        databaseHelper = new DatabaseHelper(ActivitatGuitarraacustica.this);

        //Usign cursor from reatriving data form the the database, using id and nominstrument for knowing wich table and what primary key we need to retrive the data
         Cursor cursor = databaseHelper.GetAllData("guitarres", 4);
         //Start the recycerView with and add to arraylist
        mRecyclerView = findViewById(R.id.recycler_view);
        List<DataModel> dataModelList = new ArrayList<>();
        //For every iteration in for we will pass the image we need to show and the activity.
        int id_imatge1 = R.drawable.guitacustica;
        String activity = "ActivityGuitarraAcustica1.class";
        for (int i = 1; i <= 3; i++) {
            if (i== 2){
                id_imatge1 = R.drawable.guitarraacustica2;
                 activity = "ActivityGuitarraAcustica2.class";
            } else if (i == 3){
                id_imatge1 = R.drawable.guitarraacustica3;
                 activity = "ActivityGuitarraAcustica3.class";
            }
            //For every iteration we add data to the arraylist
            dataModelList.add(new DataModel(i, cursor, id_imatge1, activity));

            if (i== 2){
                id_imatge1 = R.drawable.guitarraelectrica2;
            } else if (i == 3){
                id_imatge1 = R.drawable.guitarraelectrica3;
            }
        }

        //Finally we start other objects and layouts
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyAdapter(dataModelList, this);
        mRecyclerView.setAdapter(mAdapter);

    }

}
