package cat.dam.rocriba.sqlitetone;

import android.database.Cursor;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ActivityGuitarraEspanyola extends AppCompatActivity {

    //Inicalize private variables
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    DatabaseHelper databaseHelper;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guitarraespanyola);

        //Inicalize object
        databaseHelper = new DatabaseHelper(ActivityGuitarraEspanyola.this);
//Usign cursor from reatriving data form the the database, using id and nominstrument for knowing wich table and what primary key we need to retrive the data

         Cursor cursor = databaseHelper.GetAllData("guitarres", 7);
        //Start the recycerView with and add to arraylist
        mRecyclerView = findViewById(R.id.recycler_view);
        //For every iteration in for we will pass the image we need to show and the activity.
        List<DataModel> dataModelList = new ArrayList<>();
        String activity="ActivityGuitarraEspanyola1.class";
        int id_imatge1 = R.drawable.guitclassica;
        for (int i = 1; i <= 3; i++) {
            if (i== 2){
                id_imatge1 = R.drawable.guitarraespanyola2;
                activity="ActivityGuitarraEspanyola2.class";
            } else if (i == 3){
                id_imatge1 = R.drawable.guitarraespanyola3;
                activity="ActivityGuitarraEspanyola3.class";
            }
            //For every iteration we add data to the arraylist
            dataModelList.add(new DataModel(i, cursor, id_imatge1, activity));

        }
        //Finally we start other objects and layouts
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyAdapter(dataModelList, this);
        mRecyclerView.setAdapter(mAdapter);

    }
}
