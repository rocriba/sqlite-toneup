package cat.dam.rocriba.sqlitetone;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityPianoVertical3 extends AppCompatActivity {
    DatabaseHelper databaseHelper;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pianovertical3);
        //Inicialize all the variables we need
        TextView nominstrument =(TextView) findViewById(R.id.tv_nom30);
        ImageView imatgeinstrumment = findViewById(R.id.iv_instrument30);
        TextView descripcio = (TextView) findViewById(R.id.tv_descripcio30);
        TextView preu = (TextView) findViewById(R.id.tv_preu30);

        //Create the object for the database
        databaseHelper = new DatabaseHelper(this);
        //Extract all the data that we need from the database
        Cursor cursor = databaseHelper.GetSelectData("pianos",7);
        cursor.moveToNext();
        //Put the data in the textViews and Images
        nominstrument.setText(cursor.getString(1));
        imatgeinstrumment.setImageResource(R.drawable.pianovertical3);
        descripcio.setText(cursor.getString(3));
        String money = cursor.getInt(4) + "€";
        preu.setText(money);
    }
}
