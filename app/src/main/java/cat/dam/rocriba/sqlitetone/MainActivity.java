package cat.dam.rocriba.sqlitetone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //Inicialize variables
    EditText usuari, contrasenya;

    DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inicalizing variables
        Button login, register;
        usuari = findViewById(R.id.et_usuari);
        contrasenya = findViewById(R.id.et_contrasenya);
        login = (Button) findViewById(R.id.btn_login);
        register = (Button) findViewById(R.id.btn_register);
        databaseHelper = new DatabaseHelper(MainActivity.this);


        //Button that checks the user exists and if not trows Toast error.
        //If user exists, start the next activity
        //If user admin is loged in, start activitu admin
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usuari.getText().toString();
                String password = contrasenya.getText().toString();
                boolean isExists = databaseHelper.CheckIfUserExists(username,password);

                if (isExists){
                    if (username.equals("admin") && password.equals("admin")){
                        Intent intent = new Intent(MainActivity.this, ActivityAdmin.class);
                        startActivity(intent);

                    } else {
                        Intent activity1 = new Intent(MainActivity.this, Activity1.class);
                        startActivity(activity1);
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "USUARI O CONTRASENYA NO VÀLID", Toast.LENGTH_LONG).show();
                }

            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent_registre = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent_registre);
            }
        });
    }
}