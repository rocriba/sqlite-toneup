package cat.dam.rocriba.sqlitetone;

import android.content.Context;

import android.content.Intent;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

public class MyAdapter extends  RecyclerView.Adapter<MyAdapter.MyViewHolder>{
   //Inicalize variables
    private List<DataModel> dataModelList;
   private Context mContext;


    public static class MyViewHolder extends RecyclerView.ViewHolder{
        //Inicalize variables
        public ImageView cardImageView;
        public TextView titleTextView;
        public TextView subTitleTextView;
        public Button button_entrar;

        //Constructor
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cardImageView = itemView.findViewById(R.id.imageView);
            titleTextView = itemView.findViewById(R.id.card_title);
            subTitleTextView = itemView.findViewById(R.id.card_subtitle);
            button_entrar = itemView.findViewById(R.id.action_button_1);
            //Context context = itemView.getContext();

        }

        //Selectinf data and putting it the Textview...
        //If the users selects the btton, start the new activity with the sepecified button
        public void bindData(DataModel dataModel, Context context) {
            cardImageView.setImageResource(dataModel.getImageDrawable());
            titleTextView.setText(dataModel.getTitle());
            subTitleTextView.setText(dataModel.getSubTitle());

            button_entrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String activity = dataModel.getButton();

                    if (activity.equals("ActivityGuitarraElectrica1.class")){
                        final Intent intent;
                        intent = new Intent(context, ActivityGuitarraElectrica1.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityGuitarraElectrica2.class")){
                        final Intent intent;
                        intent = new Intent(context, ActivityGuitarraElectrica2.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityGuitarraElectrica3.class")){
                        final Intent intent;
                        intent = new Intent(context, ActivityGuitarraElectrica3.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityGuitarraAcustica1.class")){
                        final Intent intent;
                        intent = new Intent(context, ActivityGuitarraAcustica1.class);
                        context.startActivity(intent);
                    }
                    else if (activity.equals("ActivityGuitarraAcustica2.class")){
                        final Intent intent;
                        intent = new Intent(context, ActivityGuitarraAcustica2.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityGuitarraAcustica3.class")){
                        final Intent intent;
                        intent = new Intent(context, ActivityGuitarraAcustica3.class);
                        context.startActivity(intent);

                    }else if (activity.equals("ActivityGuitarraEspanyola1.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityGuitarraEspanyola1.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityGuitarraEspanyola2.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityGuitarraEspanyola2.class);
                        context.startActivity(intent);
                    }
                    else if (activity.equals("ActivityGuitarraEspanyola3.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityGuitarraEspanyola3.class);
                        context.startActivity(intent);
                    }
                    else if (activity.equals("ActivityBaixNormal1.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityBaixNormal1.class);
                        context.startActivity(intent);
                    }
                    else if (activity.equals("ActivityBaixNormal2.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityBaixNormal2.class);
                        context.startActivity(intent);
                    }
                    else if (activity.equals("ActivityBaixNormal3.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityBaixNormal3.class);
                        context.startActivity(intent);
                    }
                    else if (activity.equals("ActivityBaixAcustic1.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityBaixAcustic1.class);
                        context.startActivity(intent);
                    }
                    else if (activity.equals("ActivityBaixAcustic2.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityBaixAcustic2.class);
                        context.startActivity(intent);
                    }

                    else if (activity.equals("ActivityBaixAcustic3.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityBaixAcustic3.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityBateriaElectrica1.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityBateriaElectrica1.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityBateriaElectrica2.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityBateriaElectrica2.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityBateriaElectrica3.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityBateriaElectrica3.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityBateriaAcustica1.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityBateriaAcustica1.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityBateriaAcustica2.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityBateriaAcustica2.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityBateriaAcustica3.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityBateriaAcustica3.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityPianoEscenari1.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityPianoEscenari1.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityPianoEscenari2.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityPianoEscenari2.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityPianoEscenari3.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityPianoEscenari3.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityPianoDigital1.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityPianoDigital1.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityPianoDigital2.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityPianoDigital2.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityPianoDigital3.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityPianoDigital3.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityPianoVertical1.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityPianoVertical1.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityPianoVertical2.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityPianoVertical2.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityPianoVertical3.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityPianoVertical3.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityPianoCola1.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityPianoCola1.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityPianoCola2.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityPianoCola2.class);
                        context.startActivity(intent);
                    }else if (activity.equals("ActivityPianoCola3.class")) {
                        final Intent intent;
                        intent = new Intent(context, ActivityPianoCola3.class);
                        context.startActivity(intent);
                    }


                }
            });
        }
    }

    //Contructor
    public MyAdapter(List<DataModel> modelList, Context context){
        dataModelList = modelList;
        mContext = context;
    }
    //Public function used to create the new recyclerView
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new MyViewHolder(view);
    }

    //Function that gets the data form DataModel
    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {

        holder.bindData(dataModelList.get(position), mContext);
    }

    //Function that counts the data
    @Override
    public int getItemCount() {
       return dataModelList.size();
    }
}
