package cat.dam.rocriba.sqlitetone;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Activity1 extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    //Inicalizing variables
    ImageView imatge;
    DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);

        //Inicalizing variables
        imatge = (ImageView) findViewById(R.id.iv_imagetriada);
        databaseHelper = new DatabaseHelper(Activity1.this);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        //We Inicialize and launch the spinner and we add every type of bass instrument
        //note that we implement another library
        spinner.setOnItemSelectedListener(this);
        List<String> instruments = new ArrayList<>();
        instruments.add("Guitarra");
        instruments.add("Bateria");
        instruments.add("Baix");
        instruments.add("Piano");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, instruments);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

    }

    //Method after implementing Spinner, for every itm selected
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //We save the item selected on a string
        String item = parent.getItemAtPosition(position).toString();
        //We will make conditionals like if else and if one option equals from the Spinner, we launch the selected actvity
        if (item.equals("Bateria")){

           imatge.setImageResource(R.drawable.bateria);


            Button select = (Button) findViewById(R.id.btn_selecio);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Activity1.this, ActivityBateria.class);
                    startActivity(intent);
                }
            });

        } else if (item.equals("Guitarra")){
            imatge.setImageResource(R.drawable.guitarrabaix);
            Button select = (Button) findViewById(R.id.btn_selecio);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Activity1.this, ActivityGuitarra.class);
                    startActivity(intent);
                }
            });


        } else if (item.equals("Baix")){
            imatge.setImageResource(R.drawable.guitarrabaix);
            Button select = (Button) findViewById(R.id.btn_selecio);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Activity1.this, ActivityBaix.class);
                    startActivity(intent);
                }
            });


        } else if (item.equals("Piano")){
            imatge.setImageResource(R.drawable.piano);
            Button select = (Button) findViewById(R.id.btn_selecio);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Activity1.this, ActivityPiano.class);
                    startActivity(intent);
                }
            });

        } else{

        }

    }

    //Function that implements if nothing is selected. Nothing
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
