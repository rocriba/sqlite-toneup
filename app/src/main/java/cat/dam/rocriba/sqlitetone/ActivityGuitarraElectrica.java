package cat.dam.rocriba.sqlitetone;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ActivityGuitarraElectrica extends AppCompatActivity {

    //Inicalize private variables
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    DatabaseHelper databaseHelper;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guitarraelectrica);

        //Inicalize object
        databaseHelper = new DatabaseHelper(ActivityGuitarraElectrica.this);

        //Usign cursor from reatriving data form the the database, using id and nominstrument for knowing wich table and what primary key we need to retrive the data
        Cursor cursor = databaseHelper.GetAllData("guitarres", 1);
        //Start the recycerView with and add to arraylist
        mRecyclerView = findViewById(R.id.recycler_view);
        List<DataModel> dataModelList = new ArrayList<>();
        //For every iteration in for we will pass the image we need to show and the activity.
        int id_imatge1 = R.drawable.guitelectrica;
        String activity = "ActivityGuitarraElectrica1.class";
        for (int i = 1; i<=3; i++){
            if (i== 2){
                id_imatge1 = R.drawable.guitarraelectrica2;
                activity = "ActivityGuitarraElectrica2.class";
            } else if (i == 3){
                id_imatge1 = R.drawable.guitarraelectrica3;
                activity = "ActivityGuitarraElectrica3.class";

            }
            //For every iteration we add data to the arraylist
            dataModelList.add(new DataModel(i, cursor, id_imatge1,activity));
        }
        //Finally we start other objects and layouts
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MyAdapter(dataModelList, this);
        mRecyclerView.setAdapter(mAdapter);





    }
}
//https://blog.chirathr.com/android/2018/08/23/android-recycler-view/