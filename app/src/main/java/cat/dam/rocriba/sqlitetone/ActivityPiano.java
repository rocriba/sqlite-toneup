package cat.dam.rocriba.sqlitetone;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class ActivityPiano extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    //Inizialize variables
    ImageView imatge;
    DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_piano);

        //Inizialize variables
        imatge = (ImageView) findViewById(R.id.iv_piano);
        Spinner spinner = (Spinner) findViewById(R.id.spin_piano);
        //Using spinner, adding data to arraylist
        spinner.setOnItemSelectedListener(this);
        List<String> piano = new ArrayList<>();
        piano.add("escenari");
        piano.add("digitals");
        piano.add("verticals");
        piano.add("cola");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, piano);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

    }

    //Implementing method form class Adapter view, when a item is selected we do something depending in
    //wich item is selected
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        //We save in a string wich item is  selected
        String item = parent.getItemAtPosition(position).toString();
        //We compare wich type of instrument is selected, and when the button is
        //Cliced we start the respective activity
        if (item.equals("escenari")){
            imatge.setImageResource(R.drawable.pianoescenari);

            Button select = (Button) findViewById(R.id.btn_piano);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityPiano.this, ActivityPianoEscenari.class);
                    startActivity(intent);
                }
            });

        } else if (item.equals("digitals")){
            imatge.setImageResource(R.drawable.pianodigi);


            Button select = (Button) findViewById(R.id.btn_piano);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityPiano.this, ActivityPianoDigital.class);
                    startActivity(intent);
                }
            });

        } else if (item.equals("verticals")){
            imatge.setImageResource(R.drawable.pianovertical);
            Button select = (Button) findViewById(R.id.btn_piano);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityPiano.this, ActivityPianoVertical.class);
                    startActivity(intent);
                }
            });


        } else if (item.equals("cola")){
            imatge.setImageResource(R.drawable.pianocola);


            Button select = (Button) findViewById(R.id.btn_piano);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityPiano.this, ActivityPianoCola.class);
                    startActivity(intent);
                }
            });
        }

    }

    //Method that implements when nothin is selected, nothing.
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
