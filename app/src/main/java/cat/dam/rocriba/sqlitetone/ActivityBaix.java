package cat.dam.rocriba.sqlitetone;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class ActivityBaix extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    //Inicalize variables
    ImageView imatge;
    DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baix);

        //Inicalize variables
        imatge = (ImageView) findViewById(R.id.iv_baix);
        Spinner spinner = (Spinner) findViewById(R.id.spin_baix);

        //Inicalitze Spinner
        //We Inicialize and launch the spinner and we add every instrument
        //note that we implement another class
        spinner.setOnItemSelectedListener(this);
        //Setting optioins in a arraylist
        List<String> baix = new ArrayList<>();
        baix.add("normal");
        baix.add("acustic");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, baix);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

    }


    //Method after implementing Spinner, for every itm selected
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //We save the item selected on a string
        String item = parent.getItemAtPosition(position).toString();
        //We will make conditionals like if else and if one option equals from the Spinner, we launch the selected actvity
        if (item.equals("normal")){
            imatge.setImageResource(R.drawable.baix);
            Button select = (Button) findViewById(R.id.btn_selbaix);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityBaix.this, ActivityBaixNormal.class);
                    startActivity(intent);
                }
            });

        } else if (item.equals("acustic")){
            imatge.setImageResource(R.drawable.baixacustic);
            Button select = (Button) findViewById(R.id.btn_selbaix);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityBaix.this, ActivityBaixAcustic.class);
                    startActivity(intent);
                }
            });

        }

    }

    //Function that implements if nothing is selected. Nothing
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
