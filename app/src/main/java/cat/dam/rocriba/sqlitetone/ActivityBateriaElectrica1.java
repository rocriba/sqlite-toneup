package cat.dam.rocriba.sqlitetone;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityBateriaElectrica1 extends AppCompatActivity {
   DatabaseHelper databaseHelper;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bateriaelectrica1);
        //Inicialize all the variables we need
        TextView nominstrument =(TextView) findViewById(R.id.tv_nom10);
        ImageView imatgeinstrumment = findViewById(R.id.iv_instrument10);
        TextView descripcio = (TextView) findViewById(R.id.tv_descripcio10);
        TextView preu = (TextView) findViewById(R.id.tv_preu10);

        //Create the object for the database
        databaseHelper = new DatabaseHelper(this);
        //Extract all the data that we need fom the database
        Cursor cursor = databaseHelper.GetSelectData("bateries",1);
        cursor.moveToNext();
        //Put the data in the textViews and Images
        nominstrument.setText(cursor.getString(1));
        imatgeinstrumment.setImageResource(R.drawable.batelectrica);
        descripcio.setText(cursor.getString(3));
        String money = cursor.getInt(4) + "€";
        preu.setText(money);
    }

    }
