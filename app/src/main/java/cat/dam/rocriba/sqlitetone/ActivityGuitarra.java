package cat.dam.rocriba.sqlitetone;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class ActivityGuitarra extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guitarra);

        //Inizialize variables
        Spinner spinner = (Spinner) findViewById(R.id.spin_guit);
//Using spinner, adding data to arraylist
        spinner.setOnItemSelectedListener(this);
        List<String> guitarra = new ArrayList<>();
        guitarra.add("electrica");
        guitarra.add("acustica");
        guitarra.add("espanyola");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, guitarra);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    //Implementing method form class Adapter view, when a item is selected we do something depending in
    //wich item is selected
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //We save in a string wich item is  selected
        String item = parent.getItemAtPosition(position).toString();
        ImageView imatge = (ImageView) findViewById(R.id.iv_guit);
        //We compare wich type of instrument is selected, and when the button is
        //Cliced we start the respective activity
        if (item.equals("electrica")){
            imatge.setImageResource(R.drawable.guitelectrica);
            Button select = (Button) findViewById(R.id.btn_selguit);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityGuitarra.this, ActivityGuitarraElectrica.class);
                    startActivity(intent);

                }
            });
            } else if (item.equals("acustica")){
           imatge.setImageResource(R.drawable.guitacustica);

            Button select = (Button) findViewById(R.id.btn_selguit);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(ActivityGuitarra.this, ActivitatGuitarraacustica.class);
                    startActivity(intent);
                }
            });

        } else if (item.equals("espanyola")) {
            imatge.setImageResource(R.drawable.guitclassica);

            Button select = (Button) findViewById(R.id.btn_selguit);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityGuitarra.this, ActivityGuitarraEspanyola.class);
                    startActivity(intent);

                }
            });

        } else {

        }


    }

    //Method that implements when nothin is selected, nothing.
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
