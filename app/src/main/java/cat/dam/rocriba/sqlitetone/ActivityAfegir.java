package cat.dam.rocriba.sqlitetone;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

public class ActivityAfegir extends AppCompatActivity {

    Boolean internetDisponible = false;
    DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_afegir);

        //Inicialize Variables
        EditText nomInstrument = findViewById(R.id.et_nominstrument);
        EditText preuInstrument = findViewById(R.id.et_preunistrument);
        EditText descripcioInstrument = findViewById(R.id.et_descripcioinstrument);
        EditText urlImatge = findViewById(R.id.et_urlimatge);
        EditText tipusInstrument = findViewById(R.id.et_tipusintrument);
        EditText instrument = findViewById(R.id.et_instrument);
        Button afegir = (Button) findViewById(R.id.btn_afegirinstument);

        //Inicalitze objects
        databaseHelper = new DatabaseHelper(this);

        //When the button is clicked we add the data to the database
        afegir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               String nomIns = nomInstrument.getText().toString();
               String moneyIns = preuInstrument.getText().toString();
               String desIns = descripcioInstrument.getText().toString();
               String tipusIns = tipusInstrument.getText().toString();
               String ins = instrument.getText().toString();
               String url = urlImatge.getText().toString();
               Bitmap bpm = descarregarimatge(url);
                if (!databaseHelper.insertInstrument(ins, nomIns,tipusIns, desIns, moneyIns, bpm)){
                    Toast.makeText(getApplicationContext(),"ERROR EN INSERTAR L'intrument", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(),"Instrument entrat correctament", Toast.LENGTH_SHORT).show();
                }
            }
        });




    }


    //Private function that we give the url and returns the bitmap image dowload redy for be saved into the database
    private  Bitmap descarregarimatge(String src){

        try {
            DetectorConnexio detectorConnexio;
            detectorConnexio = new DetectorConnexio(getApplicationContext());
            internetDisponible = detectorConnexio.teConnexio();
            if (internetDisponible) {
                java.net.URL url = new java.net.URL(src);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            }
            return null;


        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
