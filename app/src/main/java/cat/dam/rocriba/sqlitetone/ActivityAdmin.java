package cat.dam.rocriba.sqlitetone;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class ActivityAdmin extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    ImageView imatge;
    DatabaseHelper databaseHelper;
    @Override
    /*
    ActivityAdmin does the same as @Activity1 but there's a button that launches ActivityDuo that you can add or delete instruments
    This got called when MainActivity users login as admin user
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        imatge = (ImageView) findViewById(R.id.iv_imagetriada);
        databaseHelper = new DatabaseHelper(ActivityAdmin.this);
        Button editor = (Button) findViewById(R.id.btn_admin);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);



        spinner.setOnItemSelectedListener(this);
        List<String> instruments = new ArrayList<>();
        instruments.add("Guitarra");
        instruments.add("Bateria");
        instruments.add("Baix");
        instruments.add("Piano");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, instruments);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        //Button that if is pressed launches the ActivityDuo(for editing instrument)
        editor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityAdmin.this, ActivityDuo.class);
                startActivity(intent);

            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        if (item.equals("Bateria")){

            imatge.setImageResource(R.drawable.bateria);
            Bitmap bitmap=null;


            Button select = (Button) findViewById(R.id.btn_selecio);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityAdmin.this, ActivityBateria.class);
                    startActivity(intent);
                }
            });


            //https://stackoverflow.com/questions/13854742/byte-array-of-image-into-imageview

        } else if (item.equals("Guitarra")){
            imatge.setImageResource(R.drawable.guitarrabaix);
            Button select = (Button) findViewById(R.id.btn_selecio);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityAdmin.this, ActivityGuitarra.class);
                    startActivity(intent);
                }
            });




        } else if (item.equals("Baix")){
            imatge.setImageResource(R.drawable.guitarrabaix);
            Button select = (Button) findViewById(R.id.btn_selecio);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityAdmin.this, ActivityBaix.class);
                    startActivity(intent);
                }
            });


        } else if (item.equals("Piano")){
            imatge.setImageResource(R.drawable.piano);
            Button select = (Button) findViewById(R.id.btn_selecio);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityAdmin.this, ActivityPiano.class);
                    startActivity(intent);
                }
            });

        } else{

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
